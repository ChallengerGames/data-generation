import random
import csv
import datetime


#recuperation d une ligne i,j dans le fichier csv pour recuperer une ligne au hasard
def getElem(file, i, j):
	with open(file, 'r') as f:
		reader = csv.reader(f)
		for line in reader:
			if reader.line_num - 1 == i:
				return line[j]


c = csv.writer(open("partie.csv", "wb"))

flag = 0

while(flag != 10000):

	ide = flag

	evite = random.choice([True, False])
	#print(evite)

	if(evite==False):
		refus = random.choice([True, False])
	else: #EVITE EST TRUE
		refus = False
	#print (refus)	

	#coordonnees
	indice = random.randint(0,26559) #genere un indice random

	x = getElem("stations_metro.csv",indice,0)
	#print(x)
	y = getElem("stations_metro.csv",indice,1)
	#print(y)

	#date = datetime.datetime.now()
	jour = random.randint(1,31) #jour du mois octobre
	heure = random.randint(6,23)
	minutes = random.randint(0,59)
	secondes = random.randint(0,59)
	date = datetime.datetime(2016, 10, jour, heure, minutes, secondes)
	#print(date)

	idGame = 1 

	idUser1 = random.randint(4162,5460)
	#print(idUser1)

	idUser2 = random.randint(4162,5460)
	while(idUser2==idUser1): #SI LES IDENTIFIANTS SONT IDENTIQUES
		idUser2 = random.randint(4162,5460)
	#print(idUser2)

	if(evite == True or refus == True): #Si la partie est evite ou refusee
		idUserWin = ""
	else :	
		idUserWin = random.choice([idUser1,idUser2])
	#print(idUserWin)

	c.writerow([ide,evite,refus,date,idGame,idUser1,idUser2,idUserWin,x,y]) #on ecrit dans le fichier csv
	flag = flag + 1
